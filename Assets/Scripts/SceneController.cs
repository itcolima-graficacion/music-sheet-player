﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{

    public void loadScene(string newScene)
    {
        SceneManager.LoadScene(newScene);
    }

    public void setVisibleInputField(GameObject newGame)
    {
        newGame.gameObject.SetActive(true);
    }

    public void QuitGame()
    {
        #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
        #else
              Application.Quit();
        #endif
    }
}
