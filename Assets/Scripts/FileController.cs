﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class FileController : MonoBehaviour
{
    public Text txt;
    public Transform quarterNote;
    private string notes;
    private int[] noteList;

    void Start()
    {
        txt.text = StaticClass.CrossSceneInformation;
        Debug.Log("String from menuInicio: " + StaticClass.CrossSceneInformation);
        if (StaticClass.CrossSceneInformation != null)
        {
            displayNotes(readString(StaticClass.CrossSceneInformation));
        }
    }

    void displayNotes(string notes)
    {
        if (notes.Length > 0)
        {
            if (notes.Contains("position"))
            {
                displayWithPositions(notes);
            }
            else
            {
                displayWithInt(notes);
            }
        }
    }

    private void displayWithPositions(string notes)
    {
        Debug.Log("Parsear con posiciones");
        string[] vector = notes.Split("%"[0]);
        
        for (int i = 1; i < vector.Length; i++)
        {
            Debug.Log(vector[i]);
            Instantiate(quarterNote, StringToVector3(vector[i]), Quaternion.identity);
        }
    }

    public static Vector3 StringToVector3(string sVector)
    {
        // Remove the parentheses
        if (sVector.StartsWith("(") && sVector.EndsWith(")"))
        {
            sVector = sVector.Substring(1, sVector.Length - 2);
        }

        // split the items
        string[] sArray = sVector.Split(',');

        // store as a Vector3
        Vector3 result = new Vector3(
            float.Parse(sArray[0]),
            float.Parse(sArray[1]),
            float.Parse(sArray[2]));

        return result;
    }

    private void displayWithInt(string notes)
    {
        Debug.Log("Parsear con enteros");
        int[] noteList = parseToInt(notes);
        int inc = 0;
        for (int i = -46; i < -46 + noteList.Length; i++)
        {
            if (noteList[inc] != 0)
            {
                instantiateNoteInt(quarterNote, i, noteList[inc] * 0.3f - 1);
            }
            inc++;
        }
    }

    int[] parseToInt(string notes)
    {
        return System.Array.ConvertAll(notes.Split(','), int.Parse);
    }

    void instantiateNoteInt(Transform note, float x, float y)
    {
        Instantiate(note, new Vector3(x, y, -5f), Quaternion.identity);
    }

    string readString(string file)
    {
        StreamReader reader = new StreamReader(getPath() + file);
        string text = reader.ReadToEnd();
        Debug.Log("Notes: " + text);
        reader.Close();
        return text;
    }

    public string getPath()
    {
        return Application.streamingAssetsPath + "/";
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            saveNotesPositions();
        }

    }

    private void saveNotesPositions()
    {
        GameObject[] notes = GameObject.FindGameObjectsWithTag("musicalNote");
        if (notes.Length > 0)
        {
            string path = getPath() + StaticClass.CrossSceneInformation;
            File.Delete(path);
            string strNotes = "position%";
            StreamWriter writer = new StreamWriter(path, true);
            int noteSound = 0;

            for (int i = 0; i < notes.Length; i++)
            {
                noteSound = notes[i].GetComponent<NoteController>().noteLine;

                if (noteSound >= 0 && noteSound <= 12)
                {
                    strNotes += notes[i].transform.position;
                    if (i < notes.Length - 1)
                    {
                        strNotes += "%";
                    }
                }
            }
            Debug.Log(strNotes);
            writer.Write(strNotes);
            writer.Close();
        }
    }

    private void orderNotes()
    {
        GameObject[] notes = GameObject.FindGameObjectsWithTag("musicalNote");
        if (notes.Length > 0)
        {
            string path = getPath() + StaticClass.CrossSceneInformation;
            File.Delete(path);
            string strNotes = "";
            StreamWriter writer = new StreamWriter(path, true);
            int noteSound = 0;

            for (int i = 0; i < notes.Length; i++)
            {
                noteSound = notes[i].GetComponent<NoteController>().noteLine;
                if (noteSound >= 0 && noteSound <= 12)
                {
                    strNotes += noteSound;
                    if (i < notes.Length - 1)
                    {
                        strNotes += ",";
                    }
                }
            }
            writer.WriteLine(strNotes);
            writer.Close();
        }
    }
}


