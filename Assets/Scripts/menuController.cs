﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public Transform menu;

    public Vector3 closedPosition = new Vector3(-8.08f, 0.84f, 4.27f);
    public Vector3 openedPosition = new Vector3(-3.87f, 0.84f, 4.27f);

    public float openSpeed;
    public GameObject pos;
    public GameObject note1;
    private bool open;
    private float _hitTime;
    private float _hitTimer;
    private bool _canHit;
    

    private void Start()
    {
        openSpeed = 5;
        open = false;
        _hitTime = 3;
        _hitTimer = 0;
        _canHit = true;
    }

    private void Update()
    {
        _hitTimer += Time.deltaTime;
        if (_hitTimer > _hitTime)
            _canHit = true;

        if (open)
        {
            menu.position = Vector3.Lerp(menu.position,
                openedPosition + pos.transform.position, Time.deltaTime * openSpeed);
        }
        else
        {
            menu.position = Vector3.Lerp(menu.position,
                closedPosition + pos.transform.position, Time.deltaTime * openSpeed);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && _canHit)
        {
            if (open)
            {
                CloseMenu();
            } else
            {
                OpenMenu();
            }
        }
        _hitTimer = 0;
    }

    public void CloseMenu()
    {
        open = false;
        note1.SetActive(true);
    }

    public void OpenMenu()
    {
        open = true;
        note1.SetActive(false);

    }
}
