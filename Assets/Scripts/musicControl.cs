﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicControl : MonoBehaviour
{
    public GameObject timeLine;
    private string action;

    private void Start()
    {
        action = "";
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Other tag: " + other.gameObject.tag);
        if (other.gameObject.CompareTag("Player")){
            action = gameObject.name;
            Debug.Log("Action: " + action);

            if (action == "back")
            {
                timeLine.GetComponent<MoveController>().velocity--;
            } else if (action == "forward")
            {
                timeLine.GetComponent<MoveController>().velocity++;
            } else if (action == "stop")
            {
                timeLine.GetComponent<MoveController>().velocity = 0;
            }
            Debug.Log("Current speed: " + timeLine.GetComponent<MoveController>().velocity);
        }
    }
}
