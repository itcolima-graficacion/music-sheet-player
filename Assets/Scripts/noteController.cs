﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteController : MonoBehaviour
{
    public int noteLine;
    private Rigidbody my_rigidbody;
    void Start()
    {
        noteLine = -1;
        my_rigidbody = GetComponent<Rigidbody>();
        my_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;

    }

    private void Update()
    {
        my_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("noteLine"))
        {
            other.gameObject.SetActive(true);
            noteLine = int.Parse(other.gameObject.name);
        }
    }
}
