﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]

public class AudioController : MonoBehaviour
{
    private int idNote;
    public AudioClip note1DO;
    public AudioClip note2RE;
    public AudioClip note3MI;
    public AudioClip note4FA;
    public AudioClip note5SOL;
    public AudioClip note6LA;
    public AudioClip note7SI;
    public AudioClip note8DOM;
    public AudioClip note9REM;
    public AudioClip note10MIM;
    public AudioClip note11FAM;
    public AudioClip note12SOLM;


    AudioSource audioSource;
    

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        idNote = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("musicalNote"))
        {
            idNote = other.gameObject.GetComponent<NoteController>().noteLine;
            Debug.Log("Nota a tocar: " + idNote);
            switch (idNote)
            {
                case 1:
                    play(note1DO);
                    break;
                case 2:
                    play(note2RE);
                    break;
                case 3:
                    play(note3MI);
                    break;
                case 4:
                    play(note4FA);
                    break;
                case 5:
                    play(note5SOL);
                    break;
                case 6:
                    play(note6LA);
                    break;
                case 7:
                    play(note7SI);
                    break;
                case 8:
                    play(note8DOM);
                    break;
                case 9:
                    play(note9REM);
                    break;
                case 10:
                    play(note10MIM);
                    break;
                case 11:
                    play(note11FAM);
                    break;
                case 12:
                    play(note12SOLM);
                    break;
            }
        }
    }

    private void play(AudioClip note)
    {
        audioSource.PlayOneShot(note, 0.7F);
    }


}