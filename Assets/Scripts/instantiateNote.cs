﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateNote : MonoBehaviour
{
    public Transform note;
    public GameObject pos;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Instantiate(note, pos.transform.position, Quaternion.identity);
        }
    }
}
