﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    public float velocity;
    private void Start()
    {
        velocity = 0f;
    }

    void Update()
    {
        transform.Translate(Time.deltaTime * velocity , 0, 0);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("limit"))
        {
            velocity = 0;
            gameObject.transform.SetPositionAndRotation(new Vector3(-45.77f, 1, 2.635f), Quaternion.identity);
        }
    }
}
